module ru.mycompany.steelwheels {
    requires java.sql;
    requires javafx.controls;
    requires javafx.fxml;
    
    requires java.persistence;
    requires java.base;
    requires org.hibernate.orm.core;

    opens ru.mycompany.steelwheels to javafx.fxml;
    exports ru.mycompany.steelwheels;
}
