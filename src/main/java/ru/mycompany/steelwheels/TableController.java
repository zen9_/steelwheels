/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.mycompany.steelwheels;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import ru.mycompany.steelwheels.dbase.DBHandler;

/**
 *
 * @author zen
 */
public class TableController implements Initializable {

    @FXML
    private Button back;
    @FXML
    private TableView<Order> table;
    @FXML
    private TableColumn<Order, Integer> col_id;
    @FXML
    private TableColumn<Order, String> col_buyer;
    @FXML
    private TableColumn<Order, String> col_info;
    @FXML
    private TableColumn<Order, Double> col_price;
    @FXML
    private TableColumn<Order, Integer> col_worker;
    
        Order order = new Order();
        DBHandler dbHandler = new DBHandler();
    
    ObservableList<Order> oblist = FXCollections.observableArrayList(
        new Order(1, "Vasya", "Полный апгрейд", 5600, 1),
        new Order(2, "Bogdan", "Замена колес", 3200, 3),
        new Order(3, "Ismail", "Смена рамы", 2400, 4),
        new Order(4, "Ishabat", "Полный апгрейд", 7200, 2));    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
            col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
            col_buyer.setCellValueFactory(new PropertyValueFactory<>("buyer"));
            col_info.setCellValueFactory(new PropertyValueFactory<>("info"));
            col_price.setCellValueFactory(new PropertyValueFactory<>("price"));
            col_worker.setCellValueFactory(new PropertyValueFactory<>("worker"));
            
            table.setItems(oblist);

    }
    
    @FXML
    private void back() throws IOException {
        App.setRoot("addOrder");
    }
    
}
