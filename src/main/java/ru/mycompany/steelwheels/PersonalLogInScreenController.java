package ru.mycompany.steelwheels;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import ru.mycompany.steelwheels.App;
import ru.mycompany.steelwheels.dbase.DBHandler;

public class PersonalLogInScreenController {
    
    
    Alert alert = new Alert(Alert.AlertType.INFORMATION);

    @FXML
    private Button addPersonButton;

    @FXML
    private TextField login_login_field;

    @FXML
    private PasswordField login_password_field;

    @FXML
    private void switchToLoginScreen() throws IOException {
        App.setRoot("loginScreen");
    }

    @FXML
    private void switchToPersRegScreen() throws IOException {
        App.setRoot("personalRegScreen");
    }

    @FXML
    private void logIn() throws IOException, SQLException, ClassNotFoundException {
        
        
        String loginText = login_login_field.getText().trim();
        String loginPassword = login_password_field.getText().trim();
        
        boolean loginTextEmpty = login_login_field == null|| login_login_field.getText().trim().length() ==  0;
        boolean loginPasswordEmpty = login_password_field == null || login_password_field.getText().trim().length() == 0;

        alert.setTitle("Информация о авторизации");
        alert.setHeaderText(null);
        
        if (loginTextEmpty || loginPasswordEmpty) {
            
            alert.setContentText("Поля не заполены");
            alert.showAndWait();
            
        } else {
            loginUser(loginText, loginPassword);
        }
    }

    private void loginUser(String loginText, String loginPassword) throws SQLException, ClassNotFoundException, IOException {

        DBHandler dbHandler = new DBHandler();

        User user = new User();
        user.setLogin(loginText);
        user.setPassword(loginPassword);

        ResultSet result = dbHandler.getUser(user);
        
        alert.setTitle("Информация о авторизации");
        alert.setHeaderText(null);

        int counter = 0;

        while (result.next()) {
            counter++;

        }

        if (counter >= 1) {
            
            alert.setContentText("Добро пожаловать " + loginText);
            alert.showAndWait();
            
            App.setRoot("addOrder");

        } else {
            alert.setContentText("Пользователь не найден");
            alert.showAndWait();
        }
    }
}
