package ru.mycompany.steelwheels;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import ru.mycompany.steelwheels.dbase.DBHandler;
import ru.mycompany.steelwheels.Order;

public class LogInScreenController {
    
    DBHandler dbHandler = new DBHandler();
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    

    @FXML
    private Button addOrderButton;

    @FXML
    private TextField buyerTextField;

    @FXML
    private TextField infoTextField;

    @FXML
    private TextField priceTextField;
    
    @FXML
    private TextField worker_id;
    

    @FXML
    private void switchToPersLoginScreen() throws IOException {
        App.setRoot("personalLogInScreen");
    }
    
    @FXML
    private void onMain() throws IOException {
        App.setRoot("loginScreen");
    }
    
    @FXML
    private void onTable() throws IOException {
        App.setRoot("table");
    }
    
    @FXML
    private void addOrder() throws IOException, ClassNotFoundException, SQLException {

        double priceTextFieldValue = Double.parseDouble(priceTextField.getText());
        int worker_idValue = Integer.parseInt(worker_id.getText());

        dbHandler = new DBHandler();

        dbHandler.addOrderInfo(buyerTextField.getText(), infoTextField.getText(), priceTextFieldValue, worker_idValue);

        buyerTextField.clear();
        infoTextField.clear();
        priceTextField.clear();
        worker_id.clear();
        

        alert.setTitle("Информация о заказе");
        alert.setHeaderText(null);
        alert.setContentText("Заказ добавлен");
        alert.showAndWait();

    }
}
