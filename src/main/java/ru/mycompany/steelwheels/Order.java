/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.mycompany.steelwheels;

/**
 *
 * @author zen
 */
public class Order {
    
    private int id;
    private String buyer;
    private String info;
    private double price;
    private int worker;

    public Order(int id, String buyer, String info, double price, int worker) {
        this.id = id;
        this.buyer = buyer;
        this.info = info;
        this.price = price;
        this.worker = worker;
    }
    
    public Order() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBuyer() {
        return buyer;
    }

    public void setBuyer(String buyer) {
        this.buyer = buyer;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getWorker() {
        return worker;
    }

    public void setWorker(int worker) {
        this.worker = worker;
    }

}