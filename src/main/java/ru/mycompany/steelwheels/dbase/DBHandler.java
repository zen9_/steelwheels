/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.mycompany.steelwheels.dbase;

/**
 *
 * @author zen
 */
import ru.mycompany.steelwheels.User;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import ru.mycompany.steelwheels.Order;

public class DBHandler extends Config {


    Connection dbConnection;
    PreparedStatement prSt;

    public Connection getDbConnection() throws ClassNotFoundException, SQLException {
        String connectionString = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName;

        Class.forName("com.mysql.jdbc.Driver");
        dbConnection = DriverManager.getConnection(connectionString, dbUser, dbPass);

        return dbConnection;
    }

    public void signUpUser(User user) throws SQLException, ClassNotFoundException {

        String insert
                = "INSERT INTO "
                + Const.USER_TABLE + "("
                + Const.USER_LOGIN + ","
                + Const.USER_PASSWORD + ")"
                + "VALUES(?,?)";

        prSt = getDbConnection().prepareStatement(insert);
        prSt.setString(1, user.getLogin());
        prSt.setString(2, user.getPassword());

        prSt.executeUpdate();
    }

    public void addOrderInfo(String buyer, String info, double price, int user) throws ClassNotFoundException, SQLException {
        String insert
                = "INSERT INTO "
                + Const.ORDER_TABLE + "("
                + Const.ORDER_BUYER + ","
                + Const.ORDER_INFO  + ","
                + Const.ORDER_PRICE + ","
                + Const.ORDER_USER  + ")"
                + "VALUES(?,?,?,?)";

        prSt = getDbConnection().prepareStatement(insert);
        prSt.setString(1, buyer);
        prSt.setString(2, info);
        prSt.setDouble(3, price);
        prSt.setInt(4, user);


        prSt.executeUpdate();
    }

    public ResultSet getUser(User user) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;

        String selectUser
                = "SELECT * FROM "
                + Const.USER_TABLE + " WHERE "
                + Const.USER_LOGIN + "=? AND "
                + Const.USER_PASSWORD + "=?";

        prSt = getDbConnection().prepareStatement(selectUser);
        prSt.setString(1, user.getLogin());
        prSt.setString(2, user.getPassword());

        resSet = prSt.executeQuery();

        return resSet;
    }
    
    public ResultSet getOrder(Order order) throws SQLException, ClassNotFoundException {
        ResultSet resSet = null;
        
        String selectOrder
                = "SELECT * FROM  "
                + Const.ORDER_ID + ","
                + Const.ORDER_BUYER + ","
                + Const.ORDER_INFO  + ","
                + Const.ORDER_PRICE + ","
                + Const.ORDER_USER  + " FROM "
                + Const.ORDER_TABLE;

        prSt = getDbConnection().prepareStatement(selectOrder);
        prSt.setInt(1, order.getId());
        prSt.setString(2, order.getBuyer());
        prSt.setString(3, order.getInfo());
        prSt.setDouble(4, order.getPrice());
        prSt.setInt(5, order.getWorker());


        resSet = prSt.executeQuery();
        
        return resSet;
    }
}
